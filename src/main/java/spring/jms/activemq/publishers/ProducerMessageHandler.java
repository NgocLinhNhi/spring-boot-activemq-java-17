package spring.jms.activemq.publishers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public final class ProducerMessageHandler implements IProducer {
    private static final Logger logger = LoggerFactory.getLogger(ProducerMessageHandler.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    public void sendMessage(String queueName, final String message) {
        logger.info("Message Sent: {} To queue name: {}", message, queueName);
        jmsTemplate.send(queueName, session -> session.createTextMessage(message));
    }

    @Override
    public void sendMessage(String queueName, final Object message) {
        logger.info("Message Sent: {} To queue name: {}", message, queueName);
        jmsTemplate.send(queueName, session -> session.createObjectMessage((Serializable) message));
    }
}
