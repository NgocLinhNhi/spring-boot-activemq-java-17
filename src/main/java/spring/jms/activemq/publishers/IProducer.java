package spring.jms.activemq.publishers;

//Java 17 sealed interface class
sealed public interface IProducer permits ProducerMessageHandler {
    void sendMessage(String queueName, final String message);

    void sendMessage(String queueName, final Object message);
}
