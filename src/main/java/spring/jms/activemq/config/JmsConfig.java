package spring.jms.activemq.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import static spring.jms.activemq.constant.JMSConstant.BROKER_PASSWORD;
import static spring.jms.activemq.constant.JMSConstant.BROKER_USERNAME;

@Configuration
@EnableJms
@ComponentScan(basePackages = "spring.jms.activemq.config")
public class JmsConfig {

    private String brokerUrl ;

    //Dùng ActiveMQ quản lý Message
    @Bean
    public ActiveMQConnectionFactory connectionFactory() throws Exception {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        loadConfig();
        connectionFactory.setBrokerURL(brokerUrl);
        connectionFactory.setUserName(BROKER_USERNAME);
        connectionFactory.setPassword(BROKER_PASSWORD);
        connectionFactory.setTrustAllPackages(true);//set để send được objectMessage

        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate() throws Exception {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());

        return template;
    }

    //Dùng jmsListenerContainerFactory quản lý Message
    //Tự set auto khi app run
    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() throws Exception {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrency("3-10");
        // true: using jms topic, false: using jms queue
        factory.setPubSubDomain(false);

        return factory;
    }


    private void loadConfig() throws Exception {
        brokerUrl = LoadPropertiesFile.getInstance().loadProperties().getProperty("broker.url");
    }

}
