package spring.jms.activemq.message;

import java.io.Serializable;

//Java 16 record class
public record Student(String name, String address) implements Serializable {

}
