package spring.jms.activemq.subscribers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import spring.jms.activemq.message.Student;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

@Component
public class ConsumerMessageHandler {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerMessageHandler.class);

    //Consumer lắng nghe queue with name :  NINH_LOVE.queue
    // -> tạo từ localhost của activeMQ hoặc từ 1 publisher nào đó ( class test)

    // Xử lý xong message = NINH_LOVE.queue
    // then send tới 1 queue/Topic mới là  VIET_LOVE.queue = annotation @SendTo
    @JmsListener(destination = "NINH_LOVE.queue")
    @SendTo("VIET_LOVE.queue")
    public String receiveTextMessage(Message jsonMessage) throws JMSException {
        String messageData;
        String response = null;

        //Java 16 instanceof pattern
        if (jsonMessage instanceof TextMessage textMessage) {
            messageData = textMessage.getText();
            response = "Redirect Message " + messageData;
            logger.info("TextMessage : {}", textMessage.getText());
        }

        return response;
    }

    @JmsListener(destination = "OBJECT_MESSAGE.queue")
    public void receiveObjMessage(ObjectMessage jsonMessage) throws JMSException {

        if (jsonMessage != null) {
            Student objectMessage = (Student) jsonMessage.getObject();
            logger.info("Object Message here: {}", objectMessage.name());
        }
    }

}