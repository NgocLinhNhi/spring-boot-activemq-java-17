package spring.jms.activemq.third.party;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import spring.jms.activemq.publishers.ProducerMessageHandler;
import spring.jms.activemq.subscribers.ConsumerMessageHandler;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class ProductListener {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerMessageHandler.class);

    @Autowired
    private ProducerMessageHandler producer;

    //ProducerListener listen message from  consumer - SubscriberMessage

    // => xử lý xong message => dữ liệu sendTo Đến Queue/topic "admin.queue" = method sendMessage
    // => không phải dùng @SendTo("VIET_LOVE.queue")
    // VÍ dụ thực tế : Cơ chế xử lý message từ con Product => xong thì bắn sang con admin
    @JmsListener(destination = "VIET_LOVE.queue")
    public void receiveMessage(Message jsonMessage) throws JMSException {
        String messageData = null;

        //Java 16 instanceOf pattern
        if (jsonMessage instanceof TextMessage textMessage) {
            messageData = textMessage.getText();
            logger.info("Nhận tin nhắn Cách 2 : {}", textMessage.getText());
        }

        producer.sendMessage("admin.queue", messageData);
    }
}