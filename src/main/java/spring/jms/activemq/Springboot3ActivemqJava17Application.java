package spring.jms.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springboot3ActivemqJava17Application {

    public static void main(String[] args) {
        SpringApplication.run(Springboot3ActivemqJava17Application.class, args);
    }

}
