package spring.jms.activemq;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import spring.jms.activemq.message.Student;
import spring.jms.activemq.publishers.ProducerMessageHandler;
import spring.jms.activemq.subscribers.ConsumerMessageHandler;

@RunWith(SpringRunner.class)
@SpringBootTest
class Springboot3ActivemqJava17ApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private ProducerMessageHandler sender;


    @Autowired
    private ConsumerMessageHandler receiver;

    //Debug or xem logger để thấy được luồng chạy của JMS ->SendTo
    //Từ đây gửi 1 message cho class SubcriberMessage hứng = queue NINH_LOVE.queue
    @Test
    public void sendMessageText() {
        sender.sendMessage("NINH_LOVE.queue", "I Love You");
        //sender.sendMessage("VIET_LOVE.queue", "I Love You");
    }

    @Test
    public void sendMessageObject() {
        sender.sendMessage("OBJECT_MESSAGE.queue", createStudent());
    }

    public Student createStudent() {
        return new Student("VIET", "Nha Trang");
    }
}
